Dedub - Ads Similarity Network
===============

**Dedub** is a DNN based identifier of similar ads by employing image and text modalities. It takes as input the image or text
or both information and identifies if two ads are duplicate/similar or not.

* A complete report of the project here: `reports/report.pdf` 
* Complete documentation of the codebase: https://giannisginis.github.io/Dedub-Docs/
* For sample notebooks check `notebooks/*`

Network Architecture
--------------------

Technology stack
----------------
* Python 3.8.x: https://www.python.org/
* Tensoflow: https://www.tensorflow.org/
* Keras: https://keras.io/
* Transformers: https://huggingface.co/docs/transformers/index
* Pytest: https://docs.pytest.org/en/stable/
* Pytest-mock: https://github.com/pytest-dev/pytest-mock/
* Docker: https://www.docker.com/

Installation
-------------
The project requirements can be installed using Python's builtin package manager `pip` in a virtual environment
or by using `Docker`. Make sure that Python version `>= 3.8.x` is installed in your system (in case you will use docker you need to install
docker as well). With Python installed, change directory to a desired location and create a Python virtual environment,
with::
```bash
$ cd /path2/Dedub_venv
$ python -m venv Dedub_venv
````

Activate the virtual environment using::

```bash
$ source /path2_Dedub_venv/bin/activate
$ pip install -r requirements.txt
```


In the case of docker run the following to create the image::
```bash
$ sudo docker build -t dedub_network:1.0 .
```

Dedub Functionalities
---------------------
All the functionalities of these project are exposed to the three python run_* scripts which are located at the top
level of the project.

#### Make dataset
* This command will download the raw files based on the env var "DATA_URL' and preprocess them based on the configuration file::

    ```bash
    $ python run_make_dataset.py --config src/config/config.py
    ```

In order to be able to retrieve the data you need to create a **.env** file at the root directory and export inside of it a `DATA_URL` environmental variable
with the link to the raw data.
#### Training
* This command will train a DNN by using text or image or both modalities depending on the configuration::
    ```bash
    $ python run_train.py --config src/config/config.py
    ```
### Inference
* This command will run a prediction by choosing which network to use based on the input of arguments.
   If only titles are provided will use a DNN for text modalities, if only images are provided will use a DNN for
   image modalities and if both are provided will uses the model for all the modalities::
    ```bash
   $ python run_inference.py --config src/config/config.py
   ```
#### Run with Docker (not properly tested)
* Run everything with docker (See installation section to see how to build the image)::
    ```bash
    $ sudo docker run --name dedub_image dedub_network:1.0 python run_train.py --config src/config/config.yaml
    ```
    If image exists then
    ```bash
    $ sudo docker run dedub_network:1.0 python run_train.py --config src/config/config.yaml
    ```
    Explore the folders inside the image
    ```bash
     $ sudo docker run -it --entrypoint=/bin/bash dedub_network:2.0
    ```
  
#### Mlflow and Tensorboard
The MLflow Tracking component is an API and UI for logging parameters, code versions, metrics, and output files during training. 
It saves all the outputs of the training in the folder /mlruns. In order to visualize the outputs of the training you can run from the root dir:

```bash
$ mlflow ui
```

In the several outputs mlflow produces logs related to tensorboard. Tensorboard is a visualization toolkit which provides the visualization 
and tooling needed for machine learning experimentation. In order to start tensorboard for a specific experiments you  can run from the root dir:

```bash
$ tensorboard --logdir mlruns/0/path_to_experiment/artifacts/tensorboard_logs/
```