# -*- coding: utf-8 -*-
import yaml


class ConfigParser:
    """Config class which contains data, train and model hyperparameters

    Args:
        cfg: A yaml configuration file

    Returns:
        A Dictionary with all the corresponding fields of yaml file as keys.
    """

    def __init__(self, cfg):
        self.__config = cfg

    @property
    def config(self):
        """Returns the class attribute with loaded yaml to dictionary"""
        return self.__config

    @classmethod
    def from_yaml(cls, cfg):
        """Creates config from YAML

        Args:
              cfg: Path to yaml file
        """
        with open(cfg, "r") as ymlfile:
            params = yaml.safe_load(ymlfile)

        return cls(params)
