# -*- coding: utf-8 -*-
from typing import List, Any, Dict


class Cluster:
    """A data representation for each cluster
    Args:
        cluster: int,
        max_train_utterances: max training utterances
        train_percentage: train percentage split
        validation_percentage: validations percentage split
    """

    __all_instances = []

    def __init__(
        self,
        cluster: int,
        max_train_utterances: int = None,
        train_percentage: float = 0.8,
        validation_percentage: float = 0.2,
    ):
        self.cluster = cluster
        self.utterances = []
        self.train_percentage = train_percentage
        self.validation_percentage = validation_percentage
        self.max_train_utterances = max_train_utterances
        self.__train_entries, self.__validation_entries = None, None
        self.__train_set_split = None
        self.__validation_set_split = None
        self.__test_set_split = None

        self.__all_instances.append(self)

    def __len__(self) -> int:
        return len(self.utterances)

    def __repr__(self) -> str:
        return f"Cluster(cluster={self.cluster})"

    @property
    def all_instances(self) -> List[Any]:
        """Returns all class instances"""
        return self.__all_instances

    def _define_train(self) -> None:
        self.__train_set_split = self.utterances[: self.__train_entries]

    def _define_test(self) -> None:
        self.__test_set_split = self.utterances[self.__train_entries :]

    def _define_validation(self) -> None:
        if len(self.__train_set_split) > 2:
            self.__validation_set_split = self.__train_set_split[
                -self.__validation_entries :
            ]
            self.__train_set_split = self.__train_set_split[
                : -self.__validation_entries
            ]

    def append_utterance(self, utterance) -> None:
        """Append an utterance to the list of utterances"""
        self.utterances.append(utterance)

    def split_cluster_utterances(self) -> None:
        """Splits data to train, test, and validation"""
        if self.max_train_utterances:
            self.__train_entries = (
                round(self.train_percentage * len(self.utterances))
                if round(self.train_percentage * len(self.utterances))
                < self.max_train_utterances
                else self.max_train_utterances
            )
        else:
            self.__train_entries = round(self.train_percentage * len(self.utterances))
        self.__validation_entries = round(
            self.validation_percentage * self.__train_entries
        )
        self._define_train()
        self._define_test()
        self._define_validation()

    def splits(self) -> Dict[str, Any]:
        """Returns the splitted datasets"""

        return {
            "train_split": self.__train_set_split,
            "test_split": self.__test_set_split,
            "validation_split": self.__validation_set_split,
        }
