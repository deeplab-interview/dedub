# -*- coding: utf-8 -*-
import itertools
import pandas as pd
import glob
from src.data.preprocessing.make_dataset import MakeDataset
from src.data.preprocessing.cluster import Cluster
from src.data.preprocessing.utterance import Utterance


class DataHandler:
    """Orchestrates the preprocessing of the raw data
    Args:
        raw_data_path: path
        processed_data_path: path to save
    """

    def __init__(self, raw_data_path: str, processed_data_path: str):
        self.raw_data_path = raw_data_path
        self.images_path = f"{self.raw_data_path}/images"
        self.image_filenames = {}
        self.processed_data_path = processed_data_path
        self.clusters_bucket = {}
        self.headers = ()
        self.train_set, self.validation_set, self.test_set = [], [], []

        self.__list_all_images_in_dir()

    def __create_data_bucket(
        self,
        max_train_utterances_per_cluster: int = 5,
        train_percentage: float = 0.8,
        validation_percentage: float = 0.2,
    ) -> None:
        """Loads raw data and creates data bucket"""
        with open(f"{self.raw_data_path}/data.txt", "r+") as data_file:
            previous_cluster = 1

            for index, line in enumerate(data_file):
                if index == 0:
                    self.headers = tuple(line.split(","))
                else:
                    data_line_split = line.strip().split(";")
                    counter, title, cluster = (
                        int(data_line_split[0]),
                        data_line_split[1],
                        int(data_line_split[-1]),
                    )
                    image_name = self.image_filenames[f"{cluster}_{counter}"]
                    if previous_cluster != cluster:
                        self.clusters_bucket[
                            previous_cluster
                        ].split_cluster_utterances()
                        previous_cluster = cluster
                    if cluster not in self.clusters_bucket:
                        self.clusters_bucket[cluster] = Cluster(
                            cluster=cluster,
                            max_train_utterances=max_train_utterances_per_cluster,
                            train_percentage=train_percentage,
                            validation_percentage=validation_percentage,
                        )
                        self.clusters_bucket[cluster].append_utterance(
                            Utterance(counter, title, image_name, cluster)
                        )
                    else:
                        self.clusters_bucket[cluster].append_utterance(
                            Utterance(counter, title, image_name, cluster)
                        )
            # end of file split for the last cluster
            self.clusters_bucket[previous_cluster].split_cluster_utterances()

    def __list_all_images_in_dir(self):
        """lists all images in directory"""
        self.image_filenames = {
            "_".join(f.split("/")[-1].split("_")[0:2]): f
            for f in glob.glob(f"{self.images_path}/*")
        }

    def split_data(
        self,
        max_train_utterances_per_cluster: int = 5,
        train_percentage: float = 0.8,
        validation_percentage: float = 0.2,
    ) -> None:
        """Splits the data"""
        self.__create_data_bucket(
            max_train_utterances_per_cluster, train_percentage, validation_percentage
        )
        for cluster, cluster_instance in self.clusters_bucket.items():
            cluster_data_splits = cluster_instance.splits()

            self.train_set.append(cluster_data_splits["train_split"])
            self.test_set.append(cluster_data_splits["test_split"])

            if cluster_data_splits["validation_split"] is not None:
                self.validation_set.append(cluster_data_splits["validation_split"])

        self.train_set = list(itertools.chain.from_iterable(self.train_set))
        self.test_set = list(itertools.chain.from_iterable(self.test_set))
        self.validation_set = list(itertools.chain.from_iterable(self.validation_set))

    def make_pairs(self, save_to_csv: bool = True) -> None:
        """Make title and image pairs"""
        for data_set in [
            ("train", self.train_set),
            ("test", self.test_set),
            ("validation", self.validation_set),
        ]:

            titles_pairs, image_titles_pairs = MakeDataset(
                data_bucket=data_set[1]
            ).create_data_pairs()
            titles_pairs_df = pd.DataFrame(
                titles_pairs, columns=["label", "first_title", "pair_title"]
            )
            image_titles_pairs_df = pd.DataFrame(
                image_titles_pairs, columns=["label", "ancor_image", "pair_image"]
            )
            if save_to_csv:
                titles_pairs_df.to_csv(
                    f"{self.processed_data_path}/{data_set[0]}_title_pairs.csv",
                    index=False,
                )
                image_titles_pairs_df.to_csv(
                    f"{self.processed_data_path}/{data_set[0]}_image_pairs.csv",
                    index=False,
                )
