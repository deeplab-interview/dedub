import os
from typing import List, Any


class PathHandler:
    """Handles directories
    Args:
        paths: A list of directories
    Returns:
        None
    """

    def __init__(self, paths: List[Any]):
        self.paths = paths

    def create_paths_if_not_exists(self) -> None:
        """Creates directories if not exist"""
        for path in self.paths:
            if not os.path.exists(path):
                # Create a new directory because it does not exist
                os.makedirs(path)

    def remove_paths_if_exists(self) -> None:
        """Deletes directories if exist"""
        for path in self.paths:
            if os.path.exists(path):
                # Create a new directory because it does not exist
                os.rmdir(path)
