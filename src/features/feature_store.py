import tensorflow as tf
from typing import Tuple


class Augmentor:
    """
    A simple class for image data augmentation for tf/keras
    """

    @staticmethod
    def augment():
        """Returns the corresponding augmentation layers from keras"""
        return tf.keras.models.Sequential(
            [
                tf.keras.layers.RandomRotation(factor=0.15),
                tf.keras.layers.RandomTranslation(height_factor=0.1, width_factor=0.1),
                tf.keras.layers.RandomFlip(),
                tf.keras.layers.RandomContrast(factor=0.1),
            ],
            name="img_augmentation",
        )


class EfficientNetFeatureExtractor(tf.keras.layers.Layer):
    """Image data fearute extractor using efficient net.
    Args:
        image_size: Image dimension,
        output_dim: final dimension of embeddings,
        dropout_prob: Dropoout probability,
        include_top: A flag to include the top linear layers of enet,
        enet_weights: A flag to define the pretrained weights,
        is_efficient_net_trainable: A flag to use enet as pre-trained layer,
        **kwargs
    """

    def __init__(
        self,
        image_size: Tuple,
        output_dim: int,
        dropout_prob: float,
        include_top: bool,
        enet_weights: str = "imagenet",
        is_efficient_net_trainable: bool = False,
        **kwargs
    ):
        super(EfficientNetFeatureExtractor, self).__init__(**kwargs)
        self.image_size = image_size
        self.output_dim = output_dim
        # Freeze the pretrained weights
        self.is_efficient_net_trainable = is_efficient_net_trainable
        self.efficient_net = tf.keras.applications.EfficientNetB0(
            input_shape=self.image_size, include_top=include_top, weights=enet_weights
        )
        self.efficient_net.trainable = self.is_efficient_net_trainable
        # Rebuild top
        self.avg_pool = tf.keras.layers.GlobalAveragePooling2D(name="avg_pool")
        self.batch_norm = tf.keras.layers.BatchNormalization()
        self.dropout = tf.keras.layers.Dropout(dropout_prob, name="top_dropout")
        self.linear_layer = tf.keras.layers.Dense(
            self.output_dim, activation="softmax", name="pred"
        )

    def call(self, inputs):

        x = self.efficient_net(inputs, training=False)

        # # Rebuild top
        x = self.avg_pool(x)
        x = self.batch_norm(x)
        x = self.dropout(x)
        outputs = self.linear_layer(x)

        return outputs


class AttentionEncoderBlock(tf.keras.layers.Layer):
    def __init__(self, embed_dim, dense_dim, num_heads, **kwargs):
        super(AttentionEncoderBlock, self).__init__(**kwargs)
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.attention_1 = tf.keras.layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim, dropout=0.0
        )
        self.layer_norm_1 = tf.keras.layers.LayerNormalization()
        self.layer_norm_2 = tf.keras.layers.LayerNormalization()
        self.dense = tf.keras.layers.Dense(dense_dim, activation="relu")

    def call(self, inputs, training, mask=None):
        inputs = self.layer_norm_1(inputs)
        inputs = self.dense(inputs)

        attention_output_1 = self.attention_1(
            query=inputs,
            value=inputs,
            key=inputs,
            attention_mask=mask,
            training=training,
        )
        out_1 = self.layer_norm_2(inputs + attention_output_1)
        return out_1
