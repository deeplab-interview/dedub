from src.tests.test_main import (
    image_encoder,
    title_encoder,
    image_train_params,
    text_train_params,
    input_titles,
    input_images,
)
from src.data.data_orchestrator import DataOrchestrator
from src.data.generators.text_data_generators import BertSemanticDataGenerator
from src.data.generators.image_data_generator import ImageDataGenerator


data_orchestrator = DataOrchestrator(
    image_encoder=image_encoder,
    title_encoder=title_encoder,
    image_train_params=image_train_params,
    network_modalities="",
    paths={},
    text_train_params=text_train_params,
)

data_orchestrator.train_df_title = input_titles
data_orchestrator.train_df_image = input_images
data_orchestrator.valid_df_title = input_titles
data_orchestrator.valid_df_image = input_images


def test_data_orchestrator_text_modalities():
    """Validates if text data loader is initialised for text modality"""
    data_orchestrator.network_modalities = "text"
    data_orchestrator.build_data_generators()

    assert not isinstance(data_orchestrator.train_data, ImageDataGenerator)
    assert not isinstance(data_orchestrator.valid_data, ImageDataGenerator)
    assert isinstance(data_orchestrator.train_data, BertSemanticDataGenerator)
    assert isinstance(data_orchestrator.valid_data, BertSemanticDataGenerator)


def test_data_orchestrator_all_modalities():
    """Validates if image data loader is initialised for all modalities"""
    data_orchestrator.network_modalities = "all"
    data_orchestrator.build_data_generators()

    assert isinstance(data_orchestrator.train_data, ImageDataGenerator)
    assert isinstance(data_orchestrator.valid_data, ImageDataGenerator)
    assert not isinstance(data_orchestrator.train_data, BertSemanticDataGenerator)
    assert not isinstance(data_orchestrator.valid_data, BertSemanticDataGenerator)


def test_data_orchestrator_image_modalities():
    """Validates if text data loader is initialised for image modality"""
    data_orchestrator.network_modalities = "image"
    data_orchestrator.build_data_generators()

    assert isinstance(data_orchestrator.train_data, ImageDataGenerator)
    assert isinstance(data_orchestrator.valid_data, ImageDataGenerator)
    assert not isinstance(data_orchestrator.train_data, BertSemanticDataGenerator)
    assert not isinstance(data_orchestrator.valid_data, BertSemanticDataGenerator)
