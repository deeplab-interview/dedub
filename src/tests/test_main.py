from dataclasses import dataclass
from src.common.config_parser import ConfigParser
from datetime import datetime
import pandas as pd
import numpy as np

input_titles = pd.DataFrame(
    [["dummy_path", "dummy_path", "0"]], columns=["first_title", "pair_title", "label"]
)
input_images = pd.DataFrame(
    [["dummy_name", "dummy_name2", "0"]], columns=["ancor_image", "pair_image", "label"]
)


@dataclass
class InputsDataClass:
    first_image: str = "dummy_path"
    second_image: str = "dummy_path"
    first_title: str = "dummy_title"
    second_title: str = "dummy_title"


config = ConfigParser.from_yaml("src/config/config.yaml").config
(
    image_train_params,
    paths,
    image_encoder,
    text_train_params,
    title_encoder,
    network_modalities,
    models_dir,
    transformers_logging_level,
) = (
    config.get("train_params").get("image_dataset"),
    config.get("paths"),
    config.get("image_encoder"),
    config.get("train_params").get("text_dataset"),
    config.get("title_encoder"),
    config.get("train_params").get("network_modalities"),
    f'{config.get("paths").get("models_dir")}/{datetime.now().strftime("%d_%m_%Y-%H:%M:%S")}',
    config.get("transformers_logging").get("level"),
)


dummy_input_image_1 = np.zeros((1, 224, 224, 3))
dummy_input_image_2 = np.zeros((1, 224, 224, 3))
[input_ids, attention_masks, token_type_ids] = [
    np.zeros((1, title_encoder.get("max_length"))),
    np.zeros((1, title_encoder.get("max_length"))),
    np.zeros((1, title_encoder.get("max_length"))),
]

dummy_train_set = [
    dummy_input_image_1,
    dummy_input_image_2,
    [input_ids, attention_masks, token_type_ids],
]
dummy_validation_set = [
    dummy_input_image_1,
    dummy_input_image_2,
    [input_ids, attention_masks, token_type_ids],
]
dummy_label = np.asarray([0])
