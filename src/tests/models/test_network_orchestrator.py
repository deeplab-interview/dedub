from src.tests.test_main import image_encoder, title_encoder, paths
from src.models.siamese_network import SiameseNetwork
from src.models.semantic_similarity_networks import BertBasedUncased
from src.models.network_orchestrator import NetworkOrchestrator

network_orchestrator = NetworkOrchestrator(
    paths=paths,
    title_encoder=title_encoder,
    image_encoder=image_encoder,
    network_modalities="",
)


def test_network_orchestrator_model_initialization_text_modality():
    """Validates if bert Network is initialized for text modality"""
    network_orchestrator.network_modalities = "text"
    network_orchestrator.initialize_network()

    assert isinstance(network_orchestrator.network, BertBasedUncased)
    assert not isinstance(network_orchestrator.network, SiameseNetwork)


def test_network_orchestrator_model_initialization_image_modality():
    """Validates if Siamese Network is initialized for image modality"""
    network_orchestrator.network_modalities = "image"
    network_orchestrator.initialize_network()

    assert not isinstance(network_orchestrator.network, BertBasedUncased)
    assert isinstance(network_orchestrator.network, SiameseNetwork)


def test_network_orchestrator_model_initialization_all_modalities():
    """Validates if Fusion Network is initialized for all modalities"""
    network_orchestrator.network_modalities = "all"
    network_orchestrator.initialize_network()

    assert not isinstance(network_orchestrator.network, BertBasedUncased)
    assert isinstance(network_orchestrator.network, SiameseNetwork)
