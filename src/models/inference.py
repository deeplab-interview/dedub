from typing import Dict
import numpy as np

from src.models.network_orchestrator import NetworkOrchestrator
from src.models.utils import LRSchedule


class DedubInference:
    """Initializes and Builds the network with the corresponding modalities based on the configuration.
    Args:
        inputs_for_inference: Data generators with data to predict,
        path_to_trained_model: Filepath for the trained model weights,
        paths: A dictionary with all the corresponding directories,
        title_encoder: A dictionary with all the metadata for the title encoder,
        image_encoder: A dictionary with all the metadata for the image encoder,
        network_modalities: A flag that indicates which modalities to use
    Returns:
        Tuple (the prediction, probability of the prediction)
    """

    def __init__(
        self,
        inputs_for_inference: np.ndarray,
        path_to_trained_model: str,
        paths: Dict,
        title_encoder: Dict,
        image_encoder: Dict,
        network_modalities: str,
    ):
        self.inputs_for_inference = inputs_for_inference
        self.path_to_trained_model = path_to_trained_model
        self.paths = paths
        self.title_encoder = title_encoder
        self.image_encoder = image_encoder
        self.network_modalities = network_modalities

        self.lr_schedule = LRSchedule(
            post_warmup_learning_rate=image_encoder.get("learning_rate_params")[
                "init_lr"
            ],
            warmup_steps=3,
        )

        self.__dummy_train_set = None
        self.__dummy_validation_set = None
        self.__dummy_label = None

    def __build_dummy_input(self):
        """Builds a dummy input because we need to run an epoch for the multiclass network in order
        to retrieve the connections of the layers"""
        dummy_input_image_1 = np.zeros((1, 224, 224, 3))
        dummy_input_image_2 = np.zeros((1, 224, 224, 3))
        [input_ids, attention_masks, token_type_ids] = [
            np.zeros((1, self.title_encoder.get("max_length"))),
            np.zeros((1, self.title_encoder.get("max_length"))),
            np.zeros((1, self.title_encoder.get("max_length"))),
        ]

        self.__dummy_train_set = [
            dummy_input_image_1,
            dummy_input_image_2,
            [input_ids, attention_masks, token_type_ids],
        ]
        self.__dummy_validation_set = [
            dummy_input_image_1,
            dummy_input_image_2,
            [input_ids, attention_masks, token_type_ids],
        ]
        self.__dummy_label = np.asarray([0])

    def inference(self):
        """Runs the inference"""
        model = NetworkOrchestrator(
            paths=self.paths,
            title_encoder=self.title_encoder,
            image_encoder=self.image_encoder,
            network_modalities=self.network_modalities,
        )

        if self.network_modalities == "image" or self.network_modalities == "all":
            # builds dummy input for siamese model build
            self.__build_dummy_input()
            # siamese
            model.initialize_network()
            model.image_encoder["epochs"] = 1
            model.forward(
                train_data=self.__dummy_train_set,
                valid_data=None,
                labels=self.__dummy_label,
                lr_schedule=self.lr_schedule,
                batch_size=1,
                use_callbacks=False,
                verbose=0,
            )
            model.network.load_weights(self.path_to_trained_model)
        elif self.network_modalities == "text":
            # bert
            model.initialize_network()
            model.network.build_model()
            model.network.load_weights(self.path_to_trained_model)

        predictions = model.predict(inputs=self.inputs_for_inference[0])[0]
        if self.network_modalities == "text":
            idx = np.argmax(predictions)
            probability = predictions[idx]
            prediction = "similar" if idx == 1 else "non-similar"
        else:
            probability = 1 - predictions[0]
            prediction = "similar" if probability >= 0.8 else "non-similar"

        return prediction, probability
