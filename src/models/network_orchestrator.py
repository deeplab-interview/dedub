from typing import Dict, Any
import numpy as np
from src.models.semantic_similarity_networks import BertBasedUncased
from src.models.siamese_network import SiameseNetwork
from src.models.utils import LRSchedule


class NetworkOrchestrator:
    """Orchestrates the functionalities of the network and chooses the suitable network based on the configuration
    Args:
        paths: Dict,
        title_encoder: A dictionary with all the metadata for the title encoder,
        image_encoder: A dictionary with all the metadata for the image encoder,
        network_modalities: A flag that indicates which modalities to use
        models_dir: A directory to save the model checkpoints,
    """

    def __init__(
        self,
        paths: Dict,
        title_encoder: Dict,
        image_encoder: Dict,
        network_modalities: str,
        models_dir: str = "",
    ):
        self.paths = paths
        self.title_encoder = title_encoder
        self.image_encoder = image_encoder
        self.network_modalities = network_modalities
        self.models_dir = models_dir
        self.use_text_encoder = True if self.network_modalities == "all" else False

        self.network = None

    def initialize_network(self) -> None:
        """Initializes the network based on the chosen Modalities. Bert for text only and
        Bert + Siamese for image and text"""
        if self.network_modalities == "text" or self.network_modalities == "all":
            self.network = BertBasedUncased(
                log_dir=self.paths.get("log_dir"),
                models_dir=self.models_dir,
                input_dim=self.title_encoder.get("max_length"),
                is_bert_trainable=self.title_encoder.get("is_bert_trainable"),
                dropout_prob=self.title_encoder.get("dropout_prob"),
                lstm_dim=self.title_encoder.get("lstm_dim"),
                lstm_activation=self.title_encoder.get("lstm_activation"),
                output_dim=self.title_encoder.get("output_dim"),
                loss_function=self.title_encoder.get("loss_function"),
                epochs=self.title_encoder.get("epochs"),
                metrics=self.title_encoder.get("metrics"),
                optimizer=self.title_encoder.get("optimizer"),
                learning_rate_params=self.title_encoder.get("learning_rate_params"),
            )
        else:
            self.network = None

        if self.network_modalities == "image" or self.network_modalities == "all":
            self.network = SiameseNetwork(
                image_shape=tuple(self.image_encoder.get("image_shape")),
                output_dim=self.image_encoder.get("output_dim"),
                dropout_prob_enet=self.image_encoder.get("dropout_prob_enet"),
                include_top_enet=self.image_encoder.get("include_top_enet"),
                enet_weights=self.image_encoder.get("enet_weights"),
                is_efficient_net_trainable=self.image_encoder.get(
                    "is_efficient_net_trainable"
                ),
                metrics=self.image_encoder.get("metrics"),
                optimizer=self.image_encoder.get("optimizer"),
                loss_function=self.image_encoder.get("loss_function"),
                learning_rate_params=self.image_encoder.get("learning_rate_params"),
                training_attention=self.image_encoder.get("training_attention"),
                use_text_encoder=self.use_text_encoder,
                text_encoder=self.network,
                models_dir=self.models_dir,
            )

    def forward(
        self,
        lr_schedule: LRSchedule,
        train_data: np.array,
        valid_data: np.array,
        labels: np.array = None,
        batch_size: int = None,
        use_callbacks: bool = True,
        verbose: Any = "auto",
    ) -> None:
        """Forward pass of the model
        Args:
            lr_schedule: Learning scheduler,
            train_data: training data,
            valid_data: validation data,
            labels: labels if the above are not data generators,
            batch_size: batch size,
            use_callbacks: A flag to use the callbacks or not,
            verbose: verbosity,
        """
        if self.network_modalities == "image" or self.network_modalities == "all":
            self.network.compile(
                optimizer=self.network.model_optimizer(learning_rate=lr_schedule),
                loss=self.network.loss_function,
                metrics=self.network.model_metrics,
            )

            self.network.fit(
                train_data,
                y=labels,
                epochs=self.image_encoder.get("epochs"),
                validation_data=valid_data,
                callbacks=[self.network.model_checkpoint_callback]
                if use_callbacks
                else None,
                batch_size=batch_size,
                validation_batch_size=batch_size,
                verbose=verbose,
            )
        elif self.network_modalities == "text":
            self.network.forward(
                train_data=train_data, validation_data=valid_data, verbose=verbose
            )

    def predict(self, inputs: np.array):
        """Inference-Prediction
        Args:
            inputs: An array of embeddings
        """
        return self.network.predict(inputs, verbose=1)

    def evaluate(self):
        """Evaluations of the model - Not implemented yet"""
        return self.network.evaluate()
