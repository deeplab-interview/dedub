import tensorflow as tf
import transformers
from typing import Dict, List, AnyStr, Any
from src.models.base_model import BaseModel
from src.data.generators.text_data_generators import BertSemanticDataGenerator


class BertBasedUncased(BaseModel):
    """semantic similarity Network with Transformers (BERT)

    Args:
          log_dir: Directory for tensorboard logs if callback is used,
          input_dim: int,
          is_bert_trainable: A flag which indicates if the bert layer is going to be trained,
          dropout_prob: Dropout probability,
          lstm_dim: Lstm Dimension,
          lstm_activation: Lstm Activation,
          output_dim: int,
          loss_function: Loss function,
          epochs: Number of epochs,
          metrics: A list of metrics,
          optimizer: A keras optimizer,
          learning_rate_params: A dictionary with the corresponding params of the lr scheduler,
          models_dir: A directory to save model checkpoints,
          use_multiprocessing: Multiprocessing during forward,
          workers: Workers during forward
    """

    def __init__(
        self,
        log_dir: str,
        input_dim: int,
        is_bert_trainable: False,
        dropout_prob: float,
        lstm_dim: int,
        lstm_activation: str,
        output_dim: int,
        loss_function: str,
        epochs: int,
        metrics: List[AnyStr],
        optimizer: AnyStr,
        learning_rate_params: Dict,
        models_dir: str = "",
        use_multiprocessing: bool = True,
        workers: int = -1,
    ):

        # initialize hyper-params
        self.log_dir = log_dir
        self.models_dir = models_dir
        self.input_dim = input_dim
        self.epochs = epochs
        self.lstm_dim = lstm_dim
        self.dropout_prob = dropout_prob
        self.learning_rate_params = learning_rate_params
        self.output_dim = output_dim
        self.is_bert_trainable = is_bert_trainable
        self.lstm_activation = lstm_activation
        self.checkpoint_path = (
            self.models_dir
            + "/bert_net_cp-epoch:{epoch:02d}-metric:{val_loss:.2f}.hdf5"
        )

        self.kera_builtin_metrics_mapper = {
            "accuracy": "acc",
            "recall": tf.keras.metrics.Recall(),
            "precision": tf.keras.metrics.Precision(),
            "auc": tf.keras.metrics.AUC(),
        }
        self.metrics = []
        for str_metric in metrics:
            self.metrics.append(self.kera_builtin_metrics_mapper[str_metric.lower()])

        self.loss_function = loss_function
        self.lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate=learning_rate_params["init_lr"],
            decay_steps=learning_rate_params["decay_steps"],
            decay_rate=learning_rate_params["decay_rate"],
            staircase=learning_rate_params["staircase"],
        )
        self.optimizers = {
            "adam": tf.keras.optimizers.Adam(learning_rate=self.lr_schedule),
            "adadelta": tf.keras.optimizers.Adadelta(learning_rate=self.lr_schedule),
            "sgd": tf.keras.optimizers.SGD(learning_rate=self.lr_schedule),
            "rmsprop": tf.keras.optimizers.RMSprop(learning_rate=self.lr_schedule),
        }
        self.optimizer = self.optimizers[optimizer.lower()]

        # callbacks for model checkpoints
        self.model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=f"{self.checkpoint_path}",
            save_weights_only=True,
            monitor="val_loss",
            mode="min",
            save_best_only=True,
        )
        self.tensorboard_callback = tf.keras.callbacks.TensorBoard(
            log_dir=f"{self.log_dir}/tensorboard_logs", histogram_freq=1
        )
        # fit params
        self.use_multiprocessing = use_multiprocessing
        self.workers = workers

        self.model = None
        self.training_history = None

    def build_model(self, as_encoder: bool = False) -> None:
        """Builds the layers of the model. If as_encoder is True then it skips the last layer and using it
        as feature extractor

            Args:
                as_encoder: A flag which indicates if it is going to run as encoder and not as classification network
        """

        # if the task is to just encode the text titles set as_encoder = True
        # initialize the network
        input_ids = tf.keras.layers.Input(
            shape=(self.input_dim,), dtype=tf.int32, name="input_ids"
        )
        # Attention masks indicates to the model which tokens should be attended to.
        attention_masks = tf.keras.layers.Input(
            shape=(self.input_dim,), dtype=tf.int32, name="attention_masks"
        )
        # Token type ids are binary masks identifying different sequences in the model.
        token_type_ids = tf.keras.layers.Input(
            shape=(self.input_dim,), dtype=tf.int32, name="token_type_ids"
        )
        # Loading pretrained BERT model.
        bert_model = transformers.TFBertModel.from_pretrained("bert-base-uncased")
        # Freeze the BERT model to reuse the pretrained features without modifying them.
        bert_model.trainable = self.is_bert_trainable

        bert_output = bert_model(
            input_ids, attention_mask=attention_masks, token_type_ids=token_type_ids
        )
        sequence_output, pooled_output = (
            bert_output.last_hidden_state,
            bert_output.pooler_output,
        )
        sequence_output_dropout = tf.keras.layers.Dropout(self.dropout_prob)(
            sequence_output
        )
        # Add trainable layers on top of frozen layers to adapt the pretrained features on the new data.
        bi_lstm = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(
                self.lstm_dim, return_sequences=True, activation=self.lstm_activation
            )
        )(sequence_output_dropout)
        # Applying hybrid pooling approach to bi_lstm sequence output.
        avg_pool = tf.keras.layers.GlobalAveragePooling1D()(bi_lstm)
        max_pool = tf.keras.layers.GlobalMaxPooling1D()(bi_lstm)

        if not as_encoder:
            concat = tf.keras.layers.concatenate([avg_pool, max_pool])
            dropout = tf.keras.layers.Dropout(self.dropout_prob)(concat)
            output = tf.keras.layers.Dense(self.output_dim, activation="softmax")(
                dropout
            )
        else:
            output = tf.keras.layers.concatenate([avg_pool, max_pool])

        self.model = tf.keras.models.Model(
            inputs=[input_ids, attention_masks, token_type_ids], outputs=output
        )

    def forward(
        self,
        train_data: BertSemanticDataGenerator,
        validation_data: BertSemanticDataGenerator,
        as_encoder: bool = False,
        use_callbacks: bool = True,
        verbose: Any = "auto",
    ) -> None:
        """Forward pass of the model
        Args:
            train_data: training data,
            validation_data: validation data,
            as_encoder: Runs as encoder,
            use_callbacks: A flag to use the callbacks or not,
            verbose: verbosity,
        """
        self.build_model(as_encoder=as_encoder)
        self.model.compile(
            optimizer=self.optimizer,
            loss=self.loss_function,
            metrics=self.metrics,
        )

        self.training_history = self.model.fit(
            train_data,
            validation_data=validation_data,
            epochs=self.epochs,
            use_multiprocessing=self.use_multiprocessing,
            workers=self.workers,
            callbacks=[self.model_checkpoint_callback] if use_callbacks else None,
            verbose=verbose,
        )

    def evaluate(self, evaluation_data: BertSemanticDataGenerator, batch_size: int):
        self.model.compile(
            optimizer=self.optimizer,
            loss=self.loss_function,
            metrics=self.metrics,
        )

        self.model.evaluate(x=evaluation_data, batch_size=batch_size, verbose=1)

    def predict(
        self,
        inference_data: BertSemanticDataGenerator,
        batch_size: int = None,
        verbose: int = 1,
    ):
        """Inference-Prediction
        Args:
            inference_data: An array with data embeddings,
            batch_size: Batch size,
            verbose: verbosity,
        """
        return self.model.predict(
            x=inference_data, batch_size=batch_size, verbose=verbose
        )

    def load_weights(self, trained_model: str):
        """Loads weights from a checkpoint.
        Args:
            trained_model: Path to trained weights
        """
        self.model.load_weights(trained_model)
