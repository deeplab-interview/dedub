from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="identifying similar ads employing image and text modalities",
    author="ioannis_gkinis",
    license="MIT",
)
