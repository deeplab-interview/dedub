import os
from datetime import datetime
import logging
import argparse
from dotenv import find_dotenv, load_dotenv

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

from src.common.logger import setup_logging
from src.common.timing import log_time_taken
from src.common.config_parser import ConfigParser
from src.models.utils import LRSchedule
from src.utils.path_handler import PathHandler
from src.models.network_orchestrator import NetworkOrchestrator
from src.data.data_orchestrator import DataOrchestrator
from src.utils.set_logging_level import LoggingLevel
import mlflow.tensorflow


logger = logging.getLogger(__loader__.name)
mlflow.tensorflow.autolog()


def main(args):
    """Interface for training"""

    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    logging.info(f"Loading Configurations")
    # create an instance of configuration file
    config = ConfigParser.from_yaml(args.config).config
    (
        image_train_params,
        paths,
        image_encoder,
        text_train_params,
        title_encoder,
        network_modalities,
        models_dir,
        transformers_logging_level,
    ) = (
        config.get("train_params").get("image_dataset"),
        config.get("paths"),
        config.get("image_encoder"),
        config.get("train_params").get("text_dataset"),
        config.get("title_encoder"),
        config.get("train_params").get("network_modalities"),
        f'{config.get("paths").get("models_dir")}/{datetime.now().strftime("%d_%m_%Y-%H:%M:%S")}',
        config.get("transformers_logging").get("level"),
    )
    # create the corresponding paths if not exists
    PathHandler([paths.get("log_dir"), models_dir]).create_paths_if_not_exists()

    # set logging level for transformers
    LoggingLevel(
        library_to_supress="transformers",
        level=transformers_logging_level,
    )()

    logging.info(f"Loading Train and Validation Data splits")
    data_orchestrator = DataOrchestrator(
        image_train_params=image_train_params,
        text_train_params=text_train_params,
        image_encoder=image_encoder,
        title_encoder=title_encoder,
        network_modalities=network_modalities,
        paths=paths,
    )
    data_orchestrator.load_datasets()

    logging.info(f"Initialize Data Generators")
    data_orchestrator.build_data_generators()

    logging.info(
        f"Initialize Model with modalities: {config.get('train_params').get('network_modalities')}"
    )
    learning_rate_params = image_encoder.get("learning_rate_params")
    lr_schedule = LRSchedule(
        post_warmup_learning_rate=learning_rate_params["init_lr"], warmup_steps=3
    )

    model = NetworkOrchestrator(
        paths=paths,
        title_encoder=title_encoder,
        image_encoder=image_encoder,
        network_modalities=network_modalities,
        models_dir=models_dir,
    )
    model.initialize_network()

    logging.info(f"Start Training")
    model.forward(
        lr_schedule=lr_schedule,
        train_data=data_orchestrator.train_data,
        valid_data=data_orchestrator.valid_data,
    )


if __name__ == "__main__":
    # Initiate the parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, help="path to yaml file")

    # Read arguments from the command line
    args = parser.parse_args()

    setup_logging()
    with log_time_taken(logger):
        main(args)
